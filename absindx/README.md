# SNES Tests by absindx

The `SNES-TestRoms` directory is
a `git subtree` mirror of [the original repository][r].
Run `./update-sources` to update the mirror to the latest version.

[SA1RamProtectionTest.sfc](./SA1RamProtectionTest.sfc)
and [SA1VersionCodeTest.sfc](./SA1VersionCodeTest.sfc)
are downloaded from the project's release page.

[r]: https://github.com/absindx/SNES-TestRoms.git

